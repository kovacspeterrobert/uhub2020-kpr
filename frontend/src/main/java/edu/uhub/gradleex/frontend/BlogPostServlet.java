package edu.uhub.gradleex.frontend;

import edu.uhub.gradleex.backend.model.BlogPost;
import edu.uhub.gradleex.backend.model.Comment;
import edu.uhub.gradleex.backend.repo.BlogPostDao;
import edu.uhub.gradleex.backend.repo.DaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Controller
@RequestMapping("/blogposts")
@Profile("!spring")
public class BlogPostServlet {
    private static final Logger LOG = LoggerFactory.getLogger(BlogPostServlet.class);
    private static final String EMPTY_STRING = "";

    @Autowired
    BlogPostDao blogPostDao;

    @PostConstruct
    public void init() {
        LOG.info("Initializing blogpost servlet");

        BlogPost blogPost = new BlogPost("title", "author", "content");
        Comment comment1 = new Comment("author1", "content1");
        Comment comment2 = new Comment("author2", "content2");
        blogPost.addComment(comment1);
        blogPost.addComment(comment2);

        blogPostDao.create(blogPost);
    }

    @GetMapping
    public ResponseEntity<?> handleGet(@RequestParam(value = "id", required = false) String blogPostId) {
        if(blogPostId == null) {
            return new ResponseEntity<>(blogPostDao.findAll(), HttpStatus.OK);
        } else {
            BlogPost item = blogPostDao.findById(Long.parseLong(blogPostId));

            if(item == null) {
                return ResponseEntity.notFound().build();
            }
            return new ResponseEntity<>(item, HttpStatus.OK);
        }
    }

    @PostMapping
    public ResponseEntity<?> handlePost(HttpServletRequest req) {
        String title = req.getParameter("title");
        String author = req.getParameter("author");
        String content = req.getParameter("content");

        if(EMPTY_STRING.equals(title) || EMPTY_STRING.equals(author) || EMPTY_STRING.equals(content)
                || title == null || author == null || content == null ) {
            LOG.warn("Not all fields are filled out");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        blogPostDao.create(new BlogPost(title, author, content));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping
    public ResponseEntity<?> handleDelete(@RequestParam("id") String blogPostId) {
        blogPostDao.delete(Long.parseLong(blogPostId));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
