package edu.uhub.gradleex.frontend;

import edu.uhub.gradleex.backend.model.BlogPost;
import edu.uhub.gradleex.backend.repo.BlogPostDao;
import edu.uhub.gradleex.backend.repo.DaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/blogpostsjsp")
public class BlogPostServletJsp extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(BlogPostServlet.class);

    @Autowired
    BlogPostDao blogPostDao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        LOG.info("Initializing blogpostjsp servlet");
    }

    @Override
    public void destroy() {
        super.destroy();
        LOG.info("Destroying blogpostjsp servlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Request arrived to blogpostjsp servlet");

        List<BlogPost> blogPosts = blogPostDao.findAll();

        req.setAttribute("blogposts", blogPosts);

        req.getRequestDispatcher("blogposts.jsp").forward(req, resp);
    }
}
