package edu.uhub.gradleex.frontend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

public class ThymeleafEngineFactory {
    private static final Logger LOG = LoggerFactory.getLogger(ThymeleafEngineFactory.class);

    private static TemplateEngine instance;

    public static synchronized  TemplateEngine getEngine() {
        if(instance == null) {
            LOG.info("Building Thymeleaf renderer");

            ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
            templateResolver.setTemplateMode(TemplateMode.HTML);
            templateResolver.setPrefix("/templates/");

            instance = new TemplateEngine();
            instance.setTemplateResolver(templateResolver);
        }

        return instance;
    }
}
