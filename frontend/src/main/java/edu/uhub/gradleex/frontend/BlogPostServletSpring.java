package edu.uhub.gradleex.frontend;

import edu.uhub.gradleex.backend.model.BlogPost;
import edu.uhub.gradleex.backend.model.Comment;
import edu.uhub.gradleex.backend.repo.BlogPostDao;
import edu.uhub.gradleex.backend.repo.spring.BlogPostSpringDataDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Controller
@RequestMapping("/blogposts")
@Profile("spring")
public class BlogPostServletSpring {
    private static final Logger LOG = LoggerFactory.getLogger(BlogPostServlet.class);
    private static final String EMPTY_STRING = "";

    @Autowired
    BlogPostSpringDataDao blogPostDao;

    @PostConstruct
    public void init() {
        LOG.info("Initializing blogpost servlet");

        BlogPost blogPost = new BlogPost("title", "author", "content");
        Comment comment1 = new Comment("author1", "content1");
        Comment comment2 = new Comment("author2", "content2");
        blogPost.addComment(comment1);
        blogPost.addComment(comment2);

        blogPostDao.save(blogPost);
    }

    @GetMapping
    public ResponseEntity<?> handleGet(@RequestParam(value = "id", required = false) String blogPostId) {
        if(blogPostId == null) {
            return new ResponseEntity<>(blogPostDao.findAll(), HttpStatus.OK);
        } else {
            Optional<BlogPost> item = blogPostDao.findById(Long.parseLong(blogPostId));

            if(!item.isPresent()) {
                return ResponseEntity.notFound().build();
            }
            return new ResponseEntity<>(item, HttpStatus.OK);
        }
    }

    @PostMapping
    public ResponseEntity<?> handlePost(HttpServletRequest req) {
        String title = req.getParameter("title");
        String author = req.getParameter("author");
        String content = req.getParameter("content");

        if(EMPTY_STRING.equals(title) || EMPTY_STRING.equals(author) || EMPTY_STRING.equals(content)
                || title == null || author == null || content == null ) {
            LOG.warn("Not all fields are filled out");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        blogPostDao.save(new BlogPost(title, author, content));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping
    public ResponseEntity<?> handleDelete(@RequestParam("id") String blogPostId) {
        blogPostDao.deleteById(Long.parseLong(blogPostId));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
