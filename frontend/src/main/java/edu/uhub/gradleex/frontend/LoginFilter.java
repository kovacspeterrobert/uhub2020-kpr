package edu.uhub.gradleex.frontend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class LoginFilter implements Filter {
    private static final Logger LOG = LoggerFactory.getLogger(LoginFilter.class);

    private List<String> excludedUrls;

    @Override
    public void init(FilterConfig config) throws ServletException {
        String excludePattern = config.getInitParameter("excludedUrls");
        excludedUrls = Arrays.asList(excludePattern.split(","));
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!(response instanceof HttpServletResponse)) {
            LOG.error("Casting error");
            return;
        }

        HttpServletResponse resp = (HttpServletResponse) response;

        String path = ((HttpServletRequest) request).getServletPath();
        if(excludedUrls.contains(path)) {
            chain.doFilter(request, response);
        } else {
            HttpServletRequest req = (HttpServletRequest)request;
            HttpSession session = req.getSession(false);
            if(session == null || session.getAttribute("username") == null) {
                LOG.info("Not logged in");
                resp.sendRedirect(req.getContextPath() + "/login");
            } else {
                chain.doFilter(request, response);
            }
        }
    }

    @Override
    public void destroy() {

    }
}
