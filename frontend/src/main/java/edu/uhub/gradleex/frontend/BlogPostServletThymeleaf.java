package edu.uhub.gradleex.frontend;

import edu.uhub.gradleex.backend.model.BlogPost;
import edu.uhub.gradleex.backend.repo.BlogPostDao;
import edu.uhub.gradleex.backend.repo.DaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/blogpoststhyme")
public class BlogPostServletThymeleaf extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(BlogPostServletThymeleaf.class);

    @Autowired
    private BlogPostDao blogPostDao;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Request arrived to thymeleaf servlet");

        List<BlogPost> blogposts = blogPostDao.findAll();

        WebContext context = new WebContext(req, resp, req.getServletContext());
        context.setVariable("blogposts", blogposts);

        ThymeleafEngineFactory.getEngine().process("blogposts.html", context, resp.getWriter());
    }
}
