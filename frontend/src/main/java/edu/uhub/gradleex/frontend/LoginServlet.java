package edu.uhub.gradleex.frontend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(LoginServlet.class);
    private String correctUsername;
    private String correctPassword;

    @Override
    public void init(ServletConfig config) throws ServletException {
        correctUsername = config.getInitParameter("Username");
        correctPassword = config.getInitParameter("Password");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username == null || password == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
        } else {
            if (username.equals(correctUsername) && password.equals(correctPassword)) {
                LOG.info("Correct login informations");
                HttpSession session = req.getSession(true);
                session.setAttribute("username", username);
                resp.sendRedirect(req.getContextPath() + "/blogposts");
            } else {
                resp.sendRedirect(req.getContextPath() + "/login");
            }
        }
    }
}
