<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <style><%@include file="styles.css"%></style>
    </head>

    <body>

    <h1>Login</h1>
    <form action="<%= request.getContextPath() %>/login" method="post">

        Username: <input type="text" name="username">
        <br />
        Password: <input type="password" name="password">
        <br />
        <input type="submit" value="Login">
    </form>

    </body>

</html>