<%@ page import="java.util.ArrayList" %>
<%@ page import="edu.uhub.gradleex.backend.model.BlogPost"    %>
<%@ page import="java.util.Iterator"                    %>

<!DOCTYPE html>
<html>

    <head>
        <title>Blogpost</title>
        <%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles.css"> --%>
        <style><%@include file="styles.css"%></style>
    </head>

    <body>
        <a href="<%= request.getContextPath() %>/logout"> Logout</a>

        <h1>Insert Item</h1>
        <form action = "<%= request.getContextPath() %>/blogposts" method = "POST">
             Author: <input type = "text" name = "author" />
             <br />
             Title: <input type = "text" name = "title" />
             <br />
             Content: <input type = "text" name = "content" />
             <br />
             <input type = "submit" value = "Submit" />
             <br />
        </form>

        <h1>Blogposts</h1>
        <%
            ArrayList<BlogPost> blogposts = (ArrayList)request.getAttribute("blogposts");
            if(request.getAttribute("blogposts") != null) {
                Iterator<BlogPost> iterator = blogposts.iterator();
        %>
            <table id="t01">
                <tr>
                    <th>ID</th>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Content</th>
                </tr>
        <%
                while(iterator.hasNext()) {
                    BlogPost item = iterator.next();
        %>
                <tr>
                    <td><%=item.getId()%></td>
                    <td><%=item.getAuthor()%></td>
                    <td><%=item.getTitle()%></td>
                    <td><%=item.getContent()%></td>
                </tr>
        <%
                }
        %>
            </table>
        <%
            }
        %>

    </body>

</html>