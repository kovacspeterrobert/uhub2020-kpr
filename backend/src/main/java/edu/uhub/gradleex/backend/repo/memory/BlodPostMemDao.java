package edu.uhub.gradleex.backend.repo.memory;

import edu.uhub.gradleex.backend.model.BlogPost;
import edu.uhub.gradleex.backend.repo.BlogPostDao;

import java.util.List;
import java.util.stream.Collectors;

public class BlodPostMemDao extends MemDao<BlogPost> implements BlogPostDao {

    @Override
    public List<BlogPost> findByAuthor(String author) {
        return entities.stream()
                .filter(blogPost -> blogPost.getAuthor().equals(author))
                .collect(Collectors.toList());
    }

    @Override
    public BlogPost findById(Long id) {
        for (BlogPost entity: entities) {
            if(entity.getId() == id) {
                return entity;
            }
        }
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
