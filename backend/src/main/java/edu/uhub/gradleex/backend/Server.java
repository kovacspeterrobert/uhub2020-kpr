package edu.uhub.gradleex.backend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Server {

    public static final Logger LOG = LoggerFactory.getLogger(Server.class);

    public void logHello() throws IOException {
        InputStream propsStream = Server.class.getResourceAsStream("/application.properties");
        Properties props = new Properties();
        props.load(propsStream);
        LOG.info("Hello " + props.get("name"));
    }
}
