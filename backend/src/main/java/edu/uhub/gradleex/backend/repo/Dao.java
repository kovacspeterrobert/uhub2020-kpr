package edu.uhub.gradleex.backend.repo;

import edu.uhub.gradleex.backend.model.BaseEntity;

import java.util.List;

public interface Dao<T extends BaseEntity> {

    List<T> findAll();

    T findById(Long id);

    void create(T entity);

    //void update(Long id, T entity);

    void delete(Long id);
}
