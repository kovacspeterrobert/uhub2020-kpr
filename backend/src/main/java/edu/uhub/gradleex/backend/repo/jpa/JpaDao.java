package edu.uhub.gradleex.backend.repo.jpa;

import edu.uhub.gradleex.backend.model.BaseEntity;
import edu.uhub.gradleex.backend.repo.Dao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JpaDao<T extends BaseEntity> implements Dao<T> {
    protected static final Logger LOG = LoggerFactory.getLogger(JpaDao.class);

    protected JpaDao() {
        super();
    }
}
