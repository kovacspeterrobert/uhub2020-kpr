package edu.uhub.gradleex.backend.repo.jdbc;

import edu.uhub.gradleex.backend.repo.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Properties;

@Component
@PropertySource("classpath:/application.properties")
public class ConnectionPool {
    private static final Logger LOG = LoggerFactory.getLogger(ConnectionPool.class);
    private static final Integer POOL_SIZE = 4;

    private final Deque<Connection> pool = new LinkedList<>();

    @Value("${db.class}")
    private String classProp;
    @Value("${db.url}")
    private String urlProp;
    @Value("${db.user}")
    private String userProp;
    @Value("${db.password}")
    private String passwordProp;

    @PostConstruct
    public void postConnectionPool() {
        try {
            Class.forName(classProp);
            String connectionUrl = urlProp;
            for (int i = 0; i < POOL_SIZE; ++i) {
                pool.push(DriverManager.getConnection(connectionUrl,
                        userProp, passwordProp));
            }
            LOG.info("Initialized pool of size {}", POOL_SIZE);
        } catch (ClassNotFoundException | SQLException e) {
            LOG.error("Connection could not be established", e);
            throw new RepositoryException("Connection could not be established", e);
        }
    }

    public synchronized Connection getConnection() {
        if (pool.isEmpty()) {
            throw new RepositoryException("No connections in pool");
        }
        LOG.info("Giving out connection from pool");
        return pool.pop();
    }

    public synchronized void returnConnection(Connection connection) {
        if (pool.size() < POOL_SIZE) {
            LOG.info("Returning connection to pool");
            pool.push(connection);
        }
    }
}
