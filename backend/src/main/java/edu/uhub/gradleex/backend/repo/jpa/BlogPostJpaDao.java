package edu.uhub.gradleex.backend.repo.jpa;

import edu.uhub.gradleex.backend.model.BlogPost;
import edu.uhub.gradleex.backend.repo.BlogPostDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.util.List;

public class BlogPostJpaDao extends JpaDao<BlogPost> implements BlogPostDao {

    @Autowired
    @Qualifier("entityManager")
    private EntityManager entityManager;

    @Override
    public List<BlogPost> findAll() {
        LOG.info("Selecting all blogposts");
        TypedQuery<BlogPost> query = entityManager.createQuery("from BlogPost", BlogPost.class);
        List<BlogPost> blogPosts = query.getResultList();

        return blogPosts;
    }

    @Override
    public List<BlogPost> findByAuthor(String author) {
        LOG.info("Selecting by author");
        TypedQuery<BlogPost> query = entityManager.createQuery("from BlogPost where author=?1", BlogPost.class);
        query.setParameter(1, author);

        return query.getResultList();
    }

    @Override
    public void create(BlogPost entity) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        try {
            if(!entityTransaction.isActive()) {
                entityTransaction.begin();

                entityManager.persist(entity);

                entityTransaction.commit();
            }
        } catch (IllegalStateException | PersistenceException | IllegalArgumentException e) {
            entityTransaction.rollback();
        }
    }

    //@Override
    public void update(BlogPost entity) {
        LOG.info("Updating blogpost with ID ", entity.getId());
        EntityTransaction entityTransaction = entityManager.getTransaction();
        try {
            if(!entityTransaction.isActive()) {
                entityTransaction.begin();

                entityManager.merge(entity);

                entityTransaction.commit();
            }
        } catch (IllegalStateException | PersistenceException | IllegalArgumentException e) {
            entityTransaction.rollback();
        }
    }

    @Override
    public BlogPost findById(Long id) {
        LOG.info("Selecting blogpost by ID ", id);
        return entityManager.find(BlogPost.class, id);
    }

    @Override
    public void delete(Long id) {
        BlogPost blogPost = findById(id);
        LOG.info("Deleting blogpost with ID ", id);
        EntityTransaction entityTransaction = entityManager.getTransaction();
        try {
            if(!entityTransaction.isActive()) {
                entityTransaction.begin();

                entityManager.remove(blogPost);

                entityTransaction.commit();
            }
        } catch (IllegalStateException | PersistenceException | IllegalArgumentException e) {
            entityTransaction.rollback();
        }
    }
}
