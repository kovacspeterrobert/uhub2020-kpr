package edu.uhub.gradleex.backend.repo.memory;

import edu.uhub.gradleex.backend.model.BaseEntity;
import edu.uhub.gradleex.backend.repo.Dao;

import java.util.ArrayList;
import java.util.List;

public abstract class MemDao<T extends BaseEntity> implements Dao<T> {

    protected List<T> entities = new ArrayList<>();

    @Override
    public java.util.List<T> findAll() {
        return entities;
    }

    @Override
    public void create(T entity) {
        entities.add(entity);
    }
}
