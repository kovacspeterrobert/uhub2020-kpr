package edu.uhub.gradleex.backend.repo.memory;

import edu.uhub.gradleex.backend.repo.BlogPostDao;
import edu.uhub.gradleex.backend.repo.DaoFactory;

public class MemDaoFactory extends DaoFactory {

    @Override
    public BlogPostDao getBlogPostDao() {
        return new BlodPostMemDao();
    }

}
