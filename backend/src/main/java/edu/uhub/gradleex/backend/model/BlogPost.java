package edu.uhub.gradleex.backend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class BlogPost extends BaseEntity {

    @Column(nullable = false)
    private String title;
    private String author;
    private String content;

    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Comment> comments = new ArrayList<>();

    public BlogPost() {
        super();
    }

    public BlogPost(String title, String author, String content) {
        super();
        this.title = title;
        this.author = author;
        this.content = content;
    }

    public BlogPost(Long id, String title, String author, String content, List comments) {
        super(id);
        this.title = title;
        this.author = author;
        this.content = content;
        this.comments = comments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void addComment(Comment comment) {
        comments.add(comment);
        comment.setBlogPost(this);
    }

    public void removeComment(Comment comment) {
        comments.remove(comment);
        comment.setBlogPost(null);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BlogPost{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", author='").append(author).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append(", comments='").append(comments).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
