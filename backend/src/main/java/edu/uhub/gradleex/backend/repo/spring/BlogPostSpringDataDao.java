package edu.uhub.gradleex.backend.repo.spring;

import edu.uhub.gradleex.backend.model.BlogPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface BlogPostSpringDataDao extends JpaRepository<BlogPost, Long> {
    Collection<BlogPost> findByAuthor(String author);
}
