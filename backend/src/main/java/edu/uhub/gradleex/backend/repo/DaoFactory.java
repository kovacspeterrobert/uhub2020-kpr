package edu.uhub.gradleex.backend.repo;

import edu.uhub.gradleex.backend.repo.jdbc.MySqlDaoFactory;
import edu.uhub.gradleex.backend.repo.jpa.JpaDaoFactory;
import edu.uhub.gradleex.backend.repo.memory.MemDaoFactory;

public abstract class DaoFactory {

    public abstract BlogPostDao getBlogPostDao();
}
