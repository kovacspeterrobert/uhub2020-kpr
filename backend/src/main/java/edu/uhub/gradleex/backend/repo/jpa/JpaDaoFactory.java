package edu.uhub.gradleex.backend.repo.jpa;

import edu.uhub.gradleex.backend.repo.BlogPostDao;
import edu.uhub.gradleex.backend.repo.DaoFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("jpa")
public class JpaDaoFactory extends DaoFactory {

    @Bean
    @Override
    public BlogPostDao getBlogPostDao() {
        return new BlogPostJpaDao();
    }
}
