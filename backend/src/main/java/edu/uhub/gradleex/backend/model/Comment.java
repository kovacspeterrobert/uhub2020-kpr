package edu.uhub.gradleex.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class Comment extends BaseEntity {

    private String author;
    private String content;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private BlogPost blogPost;

    public Comment() {
        super();
    }

    public Comment(String author, String content) {
        this.author = author;
        this.content = content;
    }

    public Comment(Long id, String author, String content, BlogPost blogPost) {
        super(id);
        this.author = author;
        this.content = content;
        this.blogPost = blogPost;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BlogPost getBlogPost() {
        return blogPost;
    }

    public void setBlogPost(BlogPost blogPost) {
        this.blogPost = blogPost;
    }

    @Override
    public String toString() {
        return "Comment{" +
                ", id=" + id + '\'' +
                "author='" + author + '\'' +
                ", content='" + content +
                '}';
    }
}
