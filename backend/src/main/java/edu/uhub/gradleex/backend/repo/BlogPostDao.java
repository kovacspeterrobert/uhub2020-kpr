package edu.uhub.gradleex.backend.repo;

import edu.uhub.gradleex.backend.model.BlogPost;

import java.util.List;

public interface BlogPostDao extends Dao<BlogPost> {

    List<BlogPost> findByAuthor(String author);

    // további blogposzt-specifikus CRUD műveletek...
}
