package edu.uhub.gradleex.backend.repo.jdbc;

import edu.uhub.gradleex.backend.model.BlogPost;
import edu.uhub.gradleex.backend.repo.BlogPostDao;
import edu.uhub.gradleex.backend.repo.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BlogPostMySqlDao extends MySqlDao<BlogPost> implements BlogPostDao {

    @Autowired
    private ConnectionPool connectionManager;

    @PostConstruct
    public void postBlogPostMySqlDao() {
        Connection connection = null;
        try {
            connection = connectionManager.getConnection();
            Statement statement = connection.createStatement();

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS BlogPost( " +
                    "id int primary key auto_increment," +
                    "title varchar(40)," +
                    "author varchar(40)," +
                    "content varchar (255));");
        } catch (SQLException e) {
            throw new RepositoryException("Create table failed", e);
        } finally {
            if(connection != null) {
                connectionManager.returnConnection(connection);
            }
        }

    }

    @Override
    public List<BlogPost> findAll() {
        Connection connection = null;
        List<BlogPost> result = new ArrayList<>();
        try {
            connection = connectionManager.getConnection();

            String query = "SELECT * FROM BlogPost";
            ResultSet resultSet = connection.createStatement().executeQuery(query);
            BlogPost blogPost;
            while (resultSet.next()) {
                blogPost = new BlogPost();
                blogPost.setId(resultSet.getLong("id"));
                blogPost.setTitle(resultSet.getString("title"));
                blogPost.setAuthor(resultSet.getString("author"));
                blogPost.setContent(resultSet.getString("content"));

                result.add(blogPost);
            }
        } catch (SQLException e) {
            throw new RepositoryException("BlogPost findAll execution failed", e);
        } finally {
            if(connection != null) {
                connectionManager.returnConnection(connection);
            }
        }
        return result;
    }

    @Override
    public void create(BlogPost entity) {
        Connection connection = null;
        try {
            connection = connectionManager.getConnection();

            String query = "INSERT INTO BlogPost (title, author, content)" +
                    "values (?,?,?)";

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, entity.getTitle());
            preparedStatement.setString(2, entity.getAuthor());
            preparedStatement.setString(3, entity.getContent());


            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RepositoryException("BlogPost insert execution failed", e);
        } finally {
            if(connection != null) {
                connectionManager.returnConnection(connection);
            }
        }
    }

    @Override
    public BlogPost findById(Long id) {
        Connection connection = null;
        BlogPost blogPost = null;

        try {
            connection = connectionManager.getConnection();

            String query = "SELECT * FROM BlogPost where ID = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                blogPost = new BlogPost();
                blogPost.setId(resultSet.getLong("id"));
                blogPost.setTitle(resultSet.getString("title"));
                blogPost.setAuthor(resultSet.getString("author"));
                blogPost.setContent(resultSet.getString("content"));
            }

        } catch (SQLException e) {
            throw new RepositoryException("BlogPost insert execution failed", e);
        } finally {
            if(connection != null) {
                connectionManager.returnConnection(connection);
            }
        }
        return blogPost;
    }

    @Override
    public List<BlogPost> findByAuthor(String author) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
