FROM gradle:jdk8 AS build-env
WORKDIR /usr/docker-gradle-multistage/
COPY . ./
RUN gradle build
	
FROM tomcat:jre8-alpine
COPY --from=build-env /usr/docker-gradle-multistage/frontend/build/libs/frontend-1.0-SNAPSHOT.war ./webapps
RUN rm -r ./webapps/ROOT/
RUN mv ./webapps/frontend-1.0-SNAPSHOT.war ./webapps/ROOT.war
